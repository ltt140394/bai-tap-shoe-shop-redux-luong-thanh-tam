import React, { Component } from 'react'
import { connect } from 'react-redux';
import { actionCreator } from './redux/constants/actions';

class Items extends Component {
    render() {
        let { name, price, image, id } = this.props.dataItem;

        return (
            <div className="col-4 my-3">
                <div className="card" style={{ height: "500px" }}>
                    <img className="card-img-top " src={image} alt="Shoe Image" />
                    <div className="card-body py-1" >
                        <h5 className="card-title text-success">{name}</h5>
                        <p className="card-text text-danger">${price}</p>
                        <button onClick={() => {
                            this.props.handleAddProduct(this.props.dataItem);
                        }} className="btn btn-warning">Add To Cart</button>
                    </div>
                </div>
            </div>
        )
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleAddProduct: (product) => {
            dispatch(actionCreator.addProduct(product));
        }
    }
}

export default connect(null, mapDispatchToProps)(Items);

