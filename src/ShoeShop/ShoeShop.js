import React, { Component } from 'react'
import { connect } from 'react-redux'
import Cart from './Cart'
import ListItems from './ListItems'

class ShoeShop extends Component {
    render() {
        return (
            <div className="container py-5">
                <h1 className="text-center text-success mb-5">NEW SHOES</h1>
                <ListItems />
                {this.props.cartList.length > 0 && <Cart />}
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        cartList: state.shoeShopReducer.cart
    }
}

export default connect(mapStateToProps)(ShoeShop);
