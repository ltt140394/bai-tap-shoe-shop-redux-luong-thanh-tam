import React, { Component } from 'react'
import { connect } from 'react-redux'
import Items from './Items'

class ListItems extends Component {
    render() {
        return (
            <div className="row">
                {this.props.productList.map((item) => {
                    return <Items dataItem={item} key={item.id} />
                })}
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        productList: state.shoeShopReducer.productList,
    }
}

export default connect(mapStateToProps)(ListItems);


