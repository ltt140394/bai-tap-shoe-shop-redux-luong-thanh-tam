import { combineReducers } from "redux";
import { shoeShopReducer } from "./shoeShopReducer";

const rootReducer = combineReducers({
    shoeShopReducer
});
export default rootReducer;