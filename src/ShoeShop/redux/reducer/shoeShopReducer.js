import { dataShoeShop } from "../../dataShoeShop";
import { ADD_PRODUCT, REMOVE_PRODUCT, UP_DOWN_QUANTITY } from "../constants/constants";

let initialState = {
    productList: dataShoeShop,
    cart: [],
}

export const shoeShopReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ADD_PRODUCT: {
            let cloneCart = [...state.cart];

            let index = cloneCart.findIndex((item) => {
                return item.id == payload.id;
            });

            if (index == -1) {
                let newProduct = { ...payload, quantity: 1, totalPrice: payload.price };
                cloneCart.push(newProduct);
            } else {
                cloneCart[index].quantity++;
                cloneCart[index].totalPrice = payload.price * cloneCart[index].quantity;
                state.quantityTxtValue = cloneCart[index].quantity;
            }

            state.cart = cloneCart;
            return { ...state };
        }

        case REMOVE_PRODUCT: {
            let cloneCart = [...state.cart];

            let index = cloneCart.findIndex((item) => {
                return item.id == payload;
            });

            cloneCart.splice(index, 1);
            state.cart = cloneCart;
            return { ...state };
        }

        case UP_DOWN_QUANTITY: {
            let cloneCart = [...state.cart];

            let index = cloneCart.findIndex((item) => {
                return item.id == payload[0];
            });

            cloneCart[index].quantity += payload[1];
            cloneCart[index].totalPrice = cloneCart[index].quantity * cloneCart[index].price;
            state.quantityTxtValue = cloneCart[index].quantity;

            if (cloneCart[index].quantity < 1) {
                cloneCart.splice(index, 1);
            }

            state.cart = cloneCart;
            return { ...state };
        }

        default: return state;
    }
}