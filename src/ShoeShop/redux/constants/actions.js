import { ADD_PRODUCT, REMOVE_PRODUCT, UP_DOWN_QUANTITY } from "./constants"

// action creator
export const actionCreator = {
    addProduct: (index) => {
        return {
            type: ADD_PRODUCT,
            payload: index,
        }
    },

    removeProduct: (index) => {
        return {
            type: REMOVE_PRODUCT,
            payload: index,
        }
    },

    upDownQuantity: (index) => {
        return {
            type: UP_DOWN_QUANTITY,
            payload: index,
        }
    },

}