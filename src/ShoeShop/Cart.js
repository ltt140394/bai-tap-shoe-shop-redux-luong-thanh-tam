import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actionCreator } from './redux/constants/actions'

class Cart extends Component {
    render() {
        return (
            <div className=" py-5">
                <h1>YOUR CART</h1>
                <table className="table">
                    <thead>
                        <tr className="font-weight-bold">
                            <td>Product Name</td>
                            <td>Price</td>
                            <td>Quantity</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.cartList.map((item) => {
                            return <tr key={item.id}>
                                <td>{item.name}</td>
                                <td>{item?.totalPrice}</td>
                                <td>
                                    <button onClick={() => {
                                        this.props.handleUpDownQuantity([item.id, -1]);
                                    }} className="btn btn-light">-</button>
                                    <span className="px-2">{item?.quantity}</span>
                                    <button onClick={() => {
                                        this.props.handleUpDownQuantity([item.id, 1]);
                                    }} className="btn btn-light">+</button>
                                </td>
                                <td>
                                    <button onClick={() => {
                                        this.props.handleRemoveProduct(item.id);
                                    }} className="btn btn-danger">Delete</button>
                                </td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        cartList: state.shoeShopReducer.cart,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleRemoveProduct: (productID) => {
            dispatch(actionCreator.removeProduct(productID));
        },

        handleUpDownQuantity: ([productID, index]) => {
            dispatch(actionCreator.upDownQuantity([productID, index]));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);

